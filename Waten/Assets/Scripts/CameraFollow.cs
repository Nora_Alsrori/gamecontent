﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
	public Vector3 offset;
	public Transform target;
	public float smoothspeed=0.125f;
	void LateUpdate(){
		Vector3	desiredPos = target.position + offset;
			transform.position =desiredPos;

	}
}
