﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatePanel : MonoBehaviour {
	public GameObject panel;

		
	private void OnCollisionEnter2D(Collision2D collisionInfo)
	{

		if (collisionInfo.gameObject.tag == "Avatar") {
			panel.SetActive (true);
			Destroy (gameObject);

		}
	}
	}
